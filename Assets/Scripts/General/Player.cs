﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public IAFlyweight IaBehaviour;
    public PathFinder pathFinder;
    private Stack<PathNode> myPath;

    private FSM fsm;

    private enum State { Idle, GoMine, Mining, GoBase, Depositing }
    private enum Event { Stop, Mine, Deposit, Drop }

    private GameObject Mine;
    private GameObject Base;

    public LayerMask miningSpotsLayer;
    public LayerMask baseLayer;

    public bool HaveGold;

    public GameObject interactionObject;
    public int chargedGold;
    public int maxChargeOfGold;
    public Mine mine;
    public Base _base;

    // Use this for initialization
    private void Awake()
    {
        pathFinder = PathFinder.instance;

        fsm = new FSM();
        fsm.Init(System.Enum.GetNames(typeof(State)).Length, System.Enum.GetNames(typeof(Event)).Length);

        //Idle to..
        fsm.SetRelation((int)State.Idle, (int)Event.Mine, (int)State.GoMine);
        fsm.SetRelation((int)State.Idle, (int)Event.Deposit, (int)State.GoBase);

        //GoMine to...
        fsm.SetRelation((int)State.GoMine, (int)Event.Stop, (int)State.Idle);
        fsm.SetRelation((int)State.GoMine, (int)Event.Mine, (int)State.Mining);

        //Mining to...
        fsm.SetRelation((int)State.Mining, (int)Event.Deposit, (int)State.GoBase);
        fsm.SetRelation((int)State.Mining, (int)Event.Stop, (int)State.Idle);

        //GoBase to...
        fsm.SetRelation((int)State.GoBase, (int)Event.Stop, (int)State.Idle);
        fsm.SetRelation((int)State.GoBase, (int)Event.Drop, (int)State.Depositing);

        //Depositing to...
        fsm.SetRelation((int)State.Depositing, (int)Event.Stop, (int)State.Idle);
        fsm.SetRelation((int)State.Depositing, (int)Event.Mine, (int)State.GoMine);

        var miningSpots = Physics.OverlapSphere(transform.position, 100, miningSpotsLayer);
        Mine = (miningSpots != null) ? miningSpots[0].gameObject : null;
        var basePlaces = Physics.OverlapSphere(transform.position, 100, baseLayer);
        Base = (basePlaces != null) ? basePlaces[0].gameObject : null;

        fsm.SetEvent((int)Event.Mine);

        new Blackboard(); //is a singleton

        Blackboard.instance.playerData.Mine = Mine;
        Blackboard.instance.playerData.Base = Base;
        Blackboard.instance.playerData.speed = 2;
        Blackboard.instance.playerData.pathFinder = pathFinder;
        Blackboard.instance.playerData.transform = transform;
        Blackboard.instance.playerData.maxChargeOfGold = maxChargeOfGold;
        Blackboard.instance.playerData.pathFinder = pathFinder;
        Blackboard.instance.playerData.baseLayer = baseLayer;
        Blackboard.instance.playerData.miningSpotsLayer = miningSpotsLayer;
        Blackboard.instance.playerData.interactionObject = interactionObject;
    }


    void Update()
    {
        IaBehaviour.root.Execute();

        //State Selector
        /*switch (fsm.GetState())
        {
            case (int)State.Idle: IdleState(); break;
            case (int)State.GoMine: GoMineState(); break;
            case (int)State.Mining: MiningState(); break;
            case (int)State.GoBase: GoBaseState(); break;
            case (int)State.Depositing: DepositingState(); break;
            default:
                break;
        }*/
    }


    void IdleState()
    {
        myPath = null;
        if (Input.GetKeyDown(KeyCode.M))
            fsm.SetEvent((int)Event.Mine);
        if (Input.GetKeyDown(KeyCode.B))
            fsm.SetEvent((int)Event.Deposit);
    }

    void GoMineState()
    {
        if (!Mine)
        {
            fsm.SetEvent((int)Event.Stop);
            return;
        }
        if (myPath == null)
            myPath = getPathTo(Mine.transform.position);
        Movement();

        if (myPath.Count <= 0)
        {
            //Resets PathFinding
            myPath = null;
            fsm.SetEvent((int)Event.Mine);
        }

        //Stops the Player current action
        if (Input.GetKeyDown(KeyCode.S))
            fsm.SetEvent((int)Event.Stop);
    }

    void MiningState()
    {
        if (!interactionObject)
        {
            //Looks for a close Mine, if not, Stops
            var go = Physics.OverlapSphere(transform.position, 0.7f, miningSpotsLayer);
            if (go.Length == 0)
            {
                if (chargedGold > 0)
                    fsm.SetEvent((int)Event.Deposit);
                else
                    fsm.SetEvent((int)Event.Stop);
                return;
            }
            interactionObject = go[0].gameObject;
        }
        if (!mine)
        {
            if (interactionObject.GetComponent<Mine>())
                mine = interactionObject.GetComponent<Mine>();
        }

        if (maxChargeOfGold > chargedGold)
        {
            HaveGold = true;
            //Precious Gooold, aaall MINE!
            chargedGold++;
            mine.GoldQuantity--;
        }
        else
        {
            interactionObject = null;
            fsm.SetEvent((int)Event.Deposit);
        }

        // Stops the Player current action
        if (Input.GetKeyDown(KeyCode.S))
        {
            interactionObject = null;
            fsm.SetEvent((int)Event.Stop);
        }
        mine = null;
    }

    void GoBaseState()
    {
        if (myPath == null)
            myPath = getPathTo(Base.transform.position);
        Movement();

        if (myPath.Count <= 0)
        {
            //Resets PathFinding
            myPath = null;
            fsm.SetEvent((int)Event.Drop);
        }

        //Stops the Player current action
        if (Input.GetKeyDown(KeyCode.S))
            fsm.SetEvent((int)Event.Stop);
    }

    void DepositingState()
    {
        HaveGold = false;
        //Looks for a close Base, if not, Stops
        var go = Physics.OverlapSphere(transform.position, 0.7f, baseLayer);
        if (go.Length > 0)
        {
            if (go[0].GetComponent<Base>())
            {
                _base = go[0].GetComponent<Base>();
                _base.GoldStored += chargedGold;
                chargedGold = 0;
                //Then go for MOOOOOOORE
                fsm.SetEvent((int)Event.Mine);
            }
        }
        else
        {
            fsm.SetEvent((int)Event.Stop);
        }
        _base = null;
    }

    Stack<PathNode> getPathTo(Vector3 placeToGo)
    {
        return pathFinder.findPathToDestiny(transform.position, placeToGo);
    }

    void Movement()
    {
        //When there is a path, movement peek the node where to go from a stack provided for the pathfinder
        if (myPath != null && myPath.Count > 0)
        {
            transform.position = Vector3.MoveTowards(transform.position, myPath.Peek().transform.position, Time.deltaTime * 2);
            if (Vector3.Distance(transform.position, myPath.Peek().transform.position) <= 0.5)
                myPath.Pop();
        }
    }
}