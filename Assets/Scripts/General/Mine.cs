﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mine : MonoBehaviour
{

    private float goldQuantity;
    public int maxGoldQuantity = 100;
    public Image imageGreen;
    public Image imageBlack;

    public float GoldQuantity
    {
        get
        {
            return goldQuantity;
        }

        set
        {
            goldQuantity = value;
            imageGreen.fillAmount = goldQuantity / maxGoldQuantity;
            if (goldQuantity <= 0)
            {
                Destroy(gameObject);
            }
        }
    }

    private void Awake()
    {
        goldQuantity = maxGoldQuantity;
    }
}
