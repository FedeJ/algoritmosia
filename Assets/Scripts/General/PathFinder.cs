﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinder : MonoBehaviour
{
    public static PathFinder instance;

    public enum PathFindingType { BreathFirst, DephtFirst, Dijkstra, Aº }

   
    public PathFindingType pathFindingType;

    public LayerMask nodeLayer;
    List<PathNode> openedNodesBreath;
    Stack<PathNode> openedNodesDepth;
    List<PathNode> closedNodes;
    public Stack<PathNode> path;
    public Material mat;

    public PathNode endNode;

    public Collider[] nearNodes;

    void Awake()
    {
        if (instance)
        {
            Destroy(this);
            return;
        }
        openedNodesBreath = new List<PathNode>();
        openedNodesDepth = new Stack<PathNode>();
        closedNodes = new List<PathNode>();

        instance = this;
    }

    public Stack<PathNode> findPathToDestiny(Vector3 from, Vector3 to)
    {
        path = new Stack<PathNode>();
        //gets the closest node to the given Position
        nearNodes = Physics.OverlapSphere(from, 0.2f, nodeLayer);
        if (nearNodes.Length > 0)
            OpenNode(nearNodes[0].GetComponent<PathNode>(), null);
        else
            return null;

        nearNodes = Physics.OverlapSphere(to, 0.2f, nodeLayer);
        if (nearNodes.Length > 0)
        {
            nearNodes[0].GetComponent<PathNode>().ChangeData(MapCreator.instance.floors[1]);
            endNode = nearNodes[0].GetComponent<PathNode>();
        }
        else
            return null;

        while (openedNodesDepth.Count != 0 || openedNodesBreath.Count != 0)
        {
            PathNode n = VisitNode();
            if (DestinyReached(n))
            {
                getPathToDestiny(n);
                ClearOpened();
                ClearClosed();
                endNode.ChangeData(MapCreator.instance.floors[3]);
                return path;
            }
            else
                OpenAdjacent(n);
        }
        ClearOpened();
        ClearClosed();
        return null;
    }

    void getPathToDestiny(PathNode n)
    {
        //Recursive functions what creates the path to destiny
        //using the parents of the nodes
        path.Push(n);
        if (n.parent)
            getPathToDestiny(n.parent);
    }

    PathNode VisitNode()
    {
        //search the first node in the openNodes List
        //remove it from openNodes and add it to closedNodes
        if (pathFindingType == PathFindingType.BreathFirst)
        {
            var auxNode = openedNodesBreath[0];
            openedNodesBreath.RemoveAt(0);
            CloseNode(auxNode);
            return auxNode;
        }
        else
        if (pathFindingType == PathFindingType.DephtFirst)
        {
            var auxNode = openedNodesDepth.Pop();
            CloseNode(auxNode);
            return auxNode;
        }
        else
        if (pathFindingType == PathFindingType.Dijkstra || pathFindingType == PathFindingType.Aº)
        {
            float minWeightNode = float.MaxValue;
            PathNode auxNode = null;
            for (int i = 0; i < openedNodesBreath.Count; i++)
            {
                if (pathFindingType == PathFindingType.Aº)
                {

                    if (minWeightNode > openedNodesBreath[i].weightToReach + Vector3.Distance(openedNodesBreath[i].transform.position, endNode.transform.position))
                    {
                        minWeightNode = openedNodesBreath[i].weightToReach + Vector3.Distance(openedNodesBreath[i].transform.position, endNode.transform.position);
                        auxNode = openedNodesBreath[i];
                    }
                }
                else
                if (minWeightNode > openedNodesBreath[i].weightToReach)
                {
                    minWeightNode = openedNodesBreath[i].weightToReach;
                    auxNode = openedNodesBreath[i];
                }
            }
            openedNodesBreath.Remove(auxNode);

            CloseNode(auxNode);
            return auxNode;
        }


        return null;
    }

    bool DestinyReached(PathNode n)
    {
        //Search if the node is "The End"
        if (n.type == 10) return true;
        else return false;
    }

    public void OpenAdjacent(PathNode n)
    {
        if (n.N && !n.N.opened && !n.N.closed && n.N.type >= 0)
            OpenNode(n.N, n);
        if (n.S && !n.S.opened && !n.S.closed && n.S.type >= 0)
            OpenNode(n.S, n);
        if (n.E && !n.E.opened && !n.E.closed && n.E.type >= 0)
            OpenNode(n.E, n);
        if (n.W && !n.W.opened && !n.W.closed && n.W.type >= 0)
            OpenNode(n.W, n);
    }

    public void OpenNode(PathNode n, PathNode parent)
    {
        //Takes 2 nodes, the first is an adjacent of the parent
        //after that add it to openNodes
        n.parent = parent;
        n.opened = true;

        if (parent)
            n.weightToReach = parent.weightToReach + n.weight;


        if (pathFindingType == PathFindingType.BreathFirst || pathFindingType == PathFindingType.Dijkstra || pathFindingType == PathFindingType.Aº)
            openedNodesBreath.Add(n);
        else if (pathFindingType == PathFindingType.DephtFirst)
            openedNodesDepth.Push(n);

    }

    public void ClearClosed()
    {
        for (int i = 0; i < closedNodes.Count; i++)
        {
            closedNodes[i].closed = false;
            closedNodes[i].parent = null;
        }
        closedNodes.Clear();
    }

    public void ClearOpened()
    {
        if (pathFindingType == PathFindingType.BreathFirst || pathFindingType == PathFindingType.Dijkstra || pathFindingType == PathFindingType.Aº)
        {
            for (int i = 0; i < openedNodesBreath.Count; i++)
            {
                openedNodesBreath[i].opened = false;
                openedNodesBreath[i].parent = null;
            }
            openedNodesBreath.Clear();
        }
        else
        if (pathFindingType == PathFindingType.DephtFirst)
        {
            while (openedNodesDepth.Count != 0)
            {
                var poped = openedNodesDepth.Pop();
                poped.opened = false;
                poped.parent = null;
            }
            openedNodesDepth.Clear();
        }
    }

    public void CloseNode(PathNode n)
    {
        if (!n) return;

        var render = n.GetComponent<Renderer>();
        render.sharedMaterial = mat;

        //Adds a given node to the closedNodes
        n.opened = false;
        n.closed = true;
        closedNodes.Add(n);
    }
}
