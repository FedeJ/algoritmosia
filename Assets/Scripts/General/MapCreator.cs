﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCreator : MonoBehaviour
{
    public static MapCreator instance;

    public GameObject prefab;
    public int cantColumns;
    public int cantRows;
    public FloorFlyweight floorData;

    public FloorFlyweight selectedFloor;

    public FloorFlyweight[] floors;
    private GameObject parent;
    private PathNode[,] matrix;

    private void Awake()
    {
        if (instance)
        {
            Destroy(this);
            return;
        }
        instance = this;
    }

    public void CreateMap() {
        var go = GameObject.Find("Enviroment");

        if (go)
            DestroyImmediate(go);

        parent = new GameObject();
        parent.name = "Enviroment";

        matrix = new PathNode[cantColumns, cantRows];

        CreateNodes();
        NodeLinker();
    }

    void CreateNodes()
    {
        int num = 0;
        for (int i = 0; i < cantColumns; i++)
            for (int j = 0; j < cantRows; j++)
            {

                var go = Instantiate(prefab, new Vector3(i * 1, 0, j * 1), Quaternion.identity, parent.transform);
                go.name = "floor"+(num++);
                PathNode n = go.AddComponent<PathNode>();
                n.col = i;
                n.row = j;
                matrix[i, j] = n;
                n.ChangeData(floorData);
            }
    }

    void NodeLinker()
    {
        for (int i = 0; i < cantColumns; i++)
            for (int j = 0; j < cantRows; j++)
            {
                if (i + 1 < cantColumns)
                    matrix[i, j].N = matrix[i + 1, j];
                else
                    matrix[i, j].N = null;
                if (i - 1 >= 0)
                    matrix[i, j].S = matrix[i - 1, j];
                else
                    matrix[i, j].S = null;
                if (j + 1 < cantRows)
                    matrix[i, j].E = matrix[i, j + 1];
                else
                    matrix[i, j].E = null;
                if (j - 1 >= 0)
                    matrix[i, j].W = matrix[i, j - 1];
                else
                    matrix[i, j].W = null;
            }
    }

    public void ChangeSelectedFloor(int i) {
        selectedFloor = floors[i];
    }
}
