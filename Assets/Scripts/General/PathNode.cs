﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathNode : MonoBehaviour
{
    public PathNode parent;
    public List<PathNode> adjacentNodes;
    public int col;
    public int row;
    public Vector3 pos;
    public int weight;
    public int weightToReach;
    public int type;
    public bool closed;
    public bool opened;

    public PathNode N, S, E, W;

    public void ChangeData(FloorFlyweight data) {
        Renderer render = GetComponent<Renderer>();
        render.sharedMaterial = data.color;
        weight = data.weight;
        name = data.Name;
        type = data.type;
        if (type >= 0)
            gameObject.layer = LayerMask.NameToLayer("Node");
        else
            gameObject.layer = LayerMask.NameToLayer("Default");
    }
}
