﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Base : MonoBehaviour {

    private float goldStored;
    public int maxStoredCapasity;
    public Image imageGreen;
    public Image imageBlack;

    public float GoldStored
    {
        get
        {
            return goldStored;
        }

        set
        {
            goldStored = value;
            imageGreen.fillAmount = goldStored / maxStoredCapasity;
        }
    }
    private void Awake()
    {
        goldStored = 0;
        imageGreen.fillAmount = 0;
    }
}
