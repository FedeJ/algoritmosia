﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndTask : Logic
{
    public override Response Execute()
    {
        Debug.Log("AndTask");

        foreach (var task in tasks)
        {
            if (task.Execute() == Response.False)
            {
                return Response.False;
            }
        }
        return Response.True;
    }
}
