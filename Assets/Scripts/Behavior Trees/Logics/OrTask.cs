﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrTask : Logic
{
    public override Response Execute()
    {
        Debug.Log("OrTask");

        foreach (var task in tasks)
        {
            if (task.Execute() == Response.True)
            {
                return Response.True;
            }
        }
        return Response.False;
    }
}
