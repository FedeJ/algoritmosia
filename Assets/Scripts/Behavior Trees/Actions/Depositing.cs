﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Depositing : Action
{
    public override Response Execute()
    {
        Debug.Log("Depositing");

        Blackboard.instance.playerData.HaveGold = false;
        //Looks for a close Base, if not, Stops
        var go = Physics.OverlapSphere(Blackboard.instance.playerData.transform.position, 0.7f, Blackboard.instance.playerData.baseLayer);
        if (go.Length > 0)
        {
            if (go[0].GetComponent<Base>())
            {
                Blackboard.instance.playerData._base = go[0].GetComponent<Base>();
                Blackboard.instance.playerData._base.GoldStored += Blackboard.instance.playerData.chargedGold;
                Blackboard.instance.playerData.chargedGold = 0;
                Blackboard.instance.playerData._base = null;
                return Response.True;
                //Then go for MOOOOOOORE
                //fsm.SetEvent((int)Event.Mine);
            }
        }
        else
        {
            return Response.False;
        }
        
        return Response.False;
    }

}
