﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mining : Action {

    public override Response Execute()
    {
        Debug.Log("Mining");

        if (!Blackboard.instance.playerData.interactionObject)
        {
            //Looks for a close Mine, if not, Stops
            var go = Physics.OverlapSphere(Blackboard.instance.playerData.transform.position, 0.7f, Blackboard.instance.playerData.miningSpotsLayer);
            if (go.Length == 0)
            {
                if (Blackboard.instance.playerData.chargedGold > 0)
                    return Response.Processing;
                else
                    return Response.False;
            }
            Blackboard.instance.playerData.interactionObject = go[0].gameObject;
        }
        if (!Blackboard.instance.playerData.mine)
        {
            if (Blackboard.instance.playerData.interactionObject.GetComponent<Mine>())
                Blackboard.instance.playerData.mine = Blackboard.instance.playerData.interactionObject.GetComponent<Mine>();
        }

        if (Blackboard.instance.playerData.maxChargeOfGold > Blackboard.instance.playerData.chargedGold)
        {
            Blackboard.instance.playerData.HaveGold = true;
            //Precious Gooold, aaall MINE!
            Blackboard.instance.playerData.chargedGold++;
            Blackboard.instance.playerData.mine.GoldQuantity--;
        }
        else
        {
            Blackboard.instance.playerData.interactionObject = null;
            Blackboard.instance.playerData.mine = null;
            return Response.False;
        }

        // Stops the Player current action
        if (Input.GetKeyDown(KeyCode.S))
        {
            Blackboard.instance.playerData.interactionObject = null;
            Blackboard.instance.playerData.mine = null;
            return Response.False;
        }
        return Response.Processing;
    }
}
