﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToMine : Action
{
    public override Response Execute()
    {
        Debug.Log("GoToMine");

        //If there is not a mine, then return
        if (!Blackboard.instance.playerData.Mine)
        {
            return Response.False;
        }
        if (Blackboard.instance.playerData.myPath == null)
        {
            Blackboard.instance.playerData.myPath = Blackboard.instance.playerData.pathFinder.findPathToDestiny(Blackboard.instance.playerData.transform.position,
                                                                                                                Blackboard.instance.playerData.Mine.transform.position);
        }
        //When there is a path, movement peek the node where to go from a stack provided for the pathfinder
        if (Blackboard.instance.playerData.myPath != null && Blackboard.instance.playerData.myPath.Count > 0)
        {
            Blackboard.instance.playerData.transform.position = Vector3.MoveTowards(Blackboard.instance.playerData.transform.position,
                                                                                    Blackboard.instance.playerData.myPath.Peek().transform.position,
                                                                                    Blackboard.instance.playerData.speed * Time.deltaTime);

            if (Vector3.Distance(Blackboard.instance.playerData.transform.position, Blackboard.instance.playerData.myPath.Peek().transform.position) <= 0.5)
                Blackboard.instance.playerData.myPath.Pop();
        }

        if (Blackboard.instance.playerData.myPath.Count <= 0)
        {
            //Resets PathFinding
            Blackboard.instance.playerData.myPath = null;
            return Response.False;
        }

        //Stops the Player current action
        if (Input.GetKeyDown(KeyCode.S))
            return Response.False;

        return Response.Processing;
    }
}
