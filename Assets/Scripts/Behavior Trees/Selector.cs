﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : TaskChildren
{
    public Selector()
    {
        taskBaseType = "Selector";
    }

    public override Response Execute()
    {
        Debug.Log("Selector");

        for (int i = 0; i < tasks.Count; i++)
            if (tasks[i].Execute() == Response.True)
                return Response.True;
        return Response.False;
    }
}
