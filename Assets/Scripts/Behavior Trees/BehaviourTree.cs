﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

public class BehaviourTree : MonoBehaviour, ISerializationCallbackReceiver
{
    [Serializable]
    public struct SerializedTaskInfo
    {
        public string taskBaseType;
        public string classType;
    }

    [Serializable]
    public struct SerializedTaskRelations
    {
        public int[] childrensIndex;
    }

    public class Task
    {
        public enum Response { True, False, Processing }

        public List<Task> tasks;
        public string taskBaseType;

        public Task parent;

        public virtual Response Execute()
        {
            return Response.True;
        }
    }

    public IAFlyweight iaFlyweight;

    public List<SerializedTaskInfo> listOfSerializedTaskInfo;
    public List<SerializedTaskRelations> listOfSerializedTaskRelations;

    private int index;
    private bool isPlaying;
    private bool serialized;

    private void Awake()
    {
        isPlaying = true;
    }

    public void OnBeforeSerialize()
    {
        Debug.Log("serializando");

        listOfSerializedTaskInfo.Clear();
        listOfSerializedTaskRelations.Clear();
        index = 0;


        if (iaFlyweight.root != null)
        {
            AddNodeToSerializedTasks(iaFlyweight.root);
            serialized = true;
        }
        if (!isPlaying)
        {
            EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
        }

    }

    int AddNodeToSerializedTasks(Task t)
    {
        var serializedTaskInfo = new SerializedTaskInfo();
        serializedTaskInfo.taskBaseType = t.taskBaseType;
        serializedTaskInfo.classType = t.GetType().ToString();

        var serializedTaskRelations = new SerializedTaskRelations();

        if (t.tasks != null)
        {
            serializedTaskRelations.childrensIndex = new int[t.tasks.Count];
            for (int i = 0; i < t.tasks.Count; i++)
            {
                index = AddNodeToSerializedTasks(t.tasks[i]);
                serializedTaskRelations.childrensIndex[i] = index;
            }
        }

        if (t.parent != null)
        {
            index++;
        }

        listOfSerializedTaskInfo.Add(serializedTaskInfo);
        listOfSerializedTaskRelations.Add(serializedTaskRelations);
        return index;
    }

    public void OnAfterDeserialize()
    {
        Debug.Log("deserializando");
        if (listOfSerializedTaskInfo.Count > 0 && serialized || iaFlyweight.root == null)
        {
            iaFlyweight.root = ReadNodeFromSerializedTasks(listOfSerializedTaskInfo.Count - 1);
        }
        addParentValue(iaFlyweight.root);
        serialized = false;
    }

    void addParentValue(Task t)
    {
        if (t == null)
            return;
        foreach (var task in t.tasks)
        {
            task.parent = t;
            addParentValue(task);
        }
    }

    Task ReadNodeFromSerializedTasks(int i)
    {
        var tasks = new List<Task>();
        var serializedTaskInfo = listOfSerializedTaskInfo[i];
        var serializedTaskRelations = listOfSerializedTaskRelations[i];

        for (int j = 0; j < serializedTaskRelations.childrensIndex.Length; j++)
        {
            var task = ReadNodeFromSerializedTasks(serializedTaskRelations.childrensIndex[j] - 1);
            tasks.Add(task);
        }

        Task t = new Task();

        switch (serializedTaskInfo.classType.ToString())
        {
            case "Depositing":
                t = new Depositing();
                break;
            case "GoToBase":
                t = new GoToBase();
                break;
            case "GoToMine":
                t = new GoToMine();
                break;
            case "Mining":
                t = new Mining();
                break;
            case "IsMineEmpty":
                t = new IsMineEmpty();
                break;
            case "IsWorkerEmpty":
                t = new IsWorkerEmpty();
                break;
            case "IsWorkerFull":
                t = new IsWorkerFull();
                break;
            case "IsNearMine":
                t = new IsNearMine();
                break;
            case "Inversor":
                t = new Inversor();
                break;
            case "AndTask":
                t = new AndTask();
                break;
            case "OrTask":
                t = new OrTask();
                break;
            case "Selector":
                t = new Selector();
                break;
            case "Sequence":
                t = new Sequence();
                break;
            default:
                Debug.LogError("Error en la serializacion: Creacion de tipo de Task");
                break;
        }
        t.taskBaseType = serializedTaskInfo.taskBaseType;
        t.tasks = tasks;
        return t;
    }
}
