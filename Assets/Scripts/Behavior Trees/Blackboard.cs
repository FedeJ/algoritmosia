﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct PlayerData {
    public int speed;
    public int chargedGold;
    public int maxChargeOfGold;
    public bool HaveGold;
    public PathFinder pathFinder;
    public Stack<PathNode> myPath;
    public Transform transform;
    public Transform target;
    public Mine mine;
    public Base _base;
    public GameObject Mine;
    public GameObject Base;
    public GameObject interactionObject;
    public LayerMask baseLayer;
    public LayerMask miningSpotsLayer;
}

public class Blackboard
{
    public static Blackboard instance;

    public PlayerData playerData;

    public Blackboard()
    {
        if (instance == null)
        {
            instance = this;
        }
        playerData = new PlayerData();
    }
}
