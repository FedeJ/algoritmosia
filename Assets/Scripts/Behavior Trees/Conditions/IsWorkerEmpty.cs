﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsWorkerEmpty : Conditional
{
    public override Response Execute()
    {
        Debug.Log("IsWorkerEmpty");

        Debug.Log(Blackboard.instance.playerData.chargedGold);
        if (Blackboard.instance.playerData.chargedGold <= 0)
            return Response.True;
        else
            return Response.False;
    }
}
