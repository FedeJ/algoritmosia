﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsMineEmpty : Conditional
{
    public override Response Execute()
    {
        Debug.Log("IsMineEmpty");

        if (Blackboard.instance.playerData.Mine == null || (Blackboard.instance.playerData.Mine != null && Blackboard.instance.playerData.Mine.GetComponent<Mine>().GoldQuantity <= 0))
            return Response.True;
        else
            return Response.False;
    }

}
