﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsWorkerFull : Conditional
{
    public override Response Execute()
    {
        Debug.Log("IsWorkerFull");

        if (Blackboard.instance.playerData.chargedGold >= Blackboard.instance.playerData.maxChargeOfGold)
            return Response.True;
        else
            return Response.False;
    }
}
