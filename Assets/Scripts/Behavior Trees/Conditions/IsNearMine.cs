﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class IsNearMine : Conditional
{
    public override Response Execute()
    {
        Debug.Log("IsNearMine");

        if (Blackboard.instance.playerData.Mine != null && Vector3.Distance(Blackboard.instance.playerData.transform.position, Blackboard.instance.playerData.Mine.transform.position) < 1)
            return Response.True;
        else
            return Response.False;
    }
}