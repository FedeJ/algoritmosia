﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inversor : Decorator {

    public override Response Execute()
    {
        Debug.Log("Inversor");

        if (tasks != null)
        {
            if (tasks[0].Execute() == Response.False)
                return Response.True;
            else
                return Response.False;
        }
        else
        {
            Debug.LogWarning("Conditional doesn't have child");
            return Response.False;
        }
    }
}
