﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sequence : TaskChildren
{
    public Sequence()
    {
        taskBaseType = "Sequence";
    }

    public override Response Execute()
    {
        Debug.Log("Sequence");
        Response auxResponse;

        for (int i = 0; i < tasks.Count; i++)
        {
            auxResponse = tasks[i].Execute();
            if (auxResponse == Response.False)
                return Response.False;
            if (auxResponse == Response.Processing)
                return Response.Processing;
        }
        return Response.True;
    }
}
