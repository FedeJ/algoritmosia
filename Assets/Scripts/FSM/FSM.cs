﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM
{
    private int[/*States*/,/*Event*/] mFSM;
    private int actualState;

    //Init the matrix for the operations
    public void Init(int numStates, int numEvents)
    {
        mFSM = new int[numStates, numEvents];

        for (int i = 0; i < numStates; i++)
            for (int j = 0; j < numEvents; j++)
                mFSM[i, j] = -1;
    }
    //Sets the relation with states, events and other states
    public void SetRelation(int fromState, int Event, int toEvent)
    {
        mFSM[fromState, Event] = toEvent;
    }

    //Set the event and change the state if there is a vinculation within the current state and the event
    public void SetEvent(int Event)
    {
        //An extra Checker 
        if (0 <= Event && Event < mFSM.GetLength(1))
        {
            //If there is no vinculation, returns
            if (mFSM[actualState, Event] == -1) { Debug.LogWarning("Error, no hay ningun estado para ese evento para el estado actual"); return; }
            //otherwise makes the change
            actualState = mFSM[actualState, Event];
        }
    }

    //Gets the actual state if the client want it
    public int GetState()
    {
        return actualState;
    }

}
