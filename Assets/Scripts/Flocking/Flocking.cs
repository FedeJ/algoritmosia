﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flocking : MonoBehaviour
{
    [Range(10.0f, 50.0f)]
    public float spawnSphere = 35.0f;
    [Range(0.1f, 1.0f)]
    public float updatesPerSecond = 0.5f;

    public Transform target;
    public int boidsToCreate;

    [Range(1.0f, 20.0f)]
    public float rangeDetection = 10;
    [Range(1.0f, 20.0f)]
    public float maxSeparation;

    [Range(1.0f, 50.0f)]
    public float speed;
    [Range(1.0f, 20.0f)]
    public float rotationSpeed;




    public GameObject boidPrefab;
    public List<Boid> boids;


    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < boidsToCreate; i++)
        {
            boids.Add(Instantiate(boidPrefab, Random.insideUnitSphere * spawnSphere, Random.rotation).GetComponent<Boid>());
        }
        InvokeRepeating("AssignNeightbordBoids", 0, updatesPerSecond);
    }

    void AssignNeightbordBoids()
    {
        foreach (var boid in boids)
        {
            boid.neightboardBoids.Clear();
            boid.maxSeparation = maxSeparation;
            boid.posTarget = target.position;
            boid.speed = speed;
            boid.rotationSpeed = rotationSpeed;
        }
        for (int i = 0; i < boids.Count; i++)
        {
            var boidA = boids[i];
            for (int j = i+1; j < boids.Count; j++)
            {
                var boidB = boids[j];
                if (Vector3.Distance(boidA.transform.position, boidB.transform.position) < rangeDetection)
                {
                    boidA.neightboardBoids.Add(boidB);
                    boidB.neightboardBoids.Add(boidA);
                }
            }
        }
    }
}
