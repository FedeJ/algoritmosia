﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boid : MonoBehaviour
{
    public List<Boid> neightboardBoids;

    Vector3 cohesion;
    Vector3 separacion;
    Vector3 alineacion;
    Vector3 velocity;
    int separacionCount;
    float distanciaRango;
    public float speed;
    public float rotationSpeed;

    [HideInInspector]
    public float maxSeparation;
    [HideInInspector]
    public Vector3 posTarget;



    private void Start()
    {
        distanciaRango = GetComponent<MeshRenderer>().bounds.extents.y * 2;
    }

    private void Update()
    {
        cohesion = transform.position;
        alineacion = transform.forward;

        foreach (var nBoids in neightboardBoids)
        {
            cohesion += nBoids.transform.position;
            alineacion += nBoids.transform.forward;

            Debug.DrawLine(transform.position, nBoids.transform.position, new Color(0, 0, 0, 0.25f));
        }

        cohesion /= neightboardBoids.Count + 1;
        alineacion /= neightboardBoids.Count + 1;

        Vector3 dirC = (cohesion - transform.position);

        var distancia = dirC.magnitude;
        var factor = (distancia / maxSeparation);

        factor = Mathf.Clamp(factor, 0, 1);

        if (neightboardBoids.Count > 0)
        {
            Debug.DrawLine(transform.position, cohesion, new Color(0, 1, 0, 0.7f));
            Debug.DrawRay(transform.position, (-dirC) * (1 - factor), new Color(1, 0, 0, 0.7f));
        }

        Vector3 dir = dirC * factor + (-dirC) * (1 - factor) + alineacion;
        dir += (posTarget - transform.position).normalized;

        dir.Normalize();

        SmoothLook(dir);

        velocity = transform.forward;

        transform.position += (velocity * speed * Time.deltaTime);
    }


    void SmoothLook(Vector3 newDirection)
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(newDirection), rotationSpeed * Time.deltaTime);
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawRay(transform.position, transform.forward);
    }
}