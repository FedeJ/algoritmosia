﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class CreateIAFlyweight
{
    [MenuItem("Assets/Create/AlgoritmosIA/Create IA Flyweight")]
    public static void CreateFlyweight()
    {
        var a = ScriptableObject.CreateInstance<IAFlyweight>();
        var p = "Assets/Data/IA Flyweight/IA Flyweight.asset";
        p = AssetDatabase.GenerateUniqueAssetPath(p);

        AssetDatabase.CreateAsset(a, p);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = a;
    }
}