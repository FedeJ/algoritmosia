﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PathNode))]
public class NodeEditor : Editor
{
    private void OnEnable()
    {
        PathNode node = (PathNode)target;
        node.ChangeData(FindObjectOfType<MapCreator>().selectedFloor);
    }
}