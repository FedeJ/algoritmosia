﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class CreateFloorFlyweight
{
    [MenuItem("Assets/Create/AlgoritmosIA/Create Floor Flyweight")]
    public static void CreateFlyweight()
    {
        var a = ScriptableObject.CreateInstance<FloorFlyweight>();
        var p = "Assets/Data/Floor Flyweight/FloorFlyweight.asset";
        p = AssetDatabase.GenerateUniqueAssetPath(p);

        AssetDatabase.CreateAsset(a, p);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = a;
    }
}