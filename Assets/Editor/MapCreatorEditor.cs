﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MapCreator))]
public class MapCreatorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        MapCreator map = (MapCreator)target;

        if (GUILayout.Button("Create Map"))
            map.CreateMap();

        if (GUILayout.Button("Begin"))
            map.ChangeSelectedFloor(0);
        if (GUILayout.Button("End"))
            map.ChangeSelectedFloor(1);
        if (GUILayout.Button("HardFloor"))
            map.ChangeSelectedFloor(2);
        if (GUILayout.Button("NormalFloor"))
            map.ChangeSelectedFloor(3);
        if (GUILayout.Button("Wall"))
            map.ChangeSelectedFloor(4);
    }
}
