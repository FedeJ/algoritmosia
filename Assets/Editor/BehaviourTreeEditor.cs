﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public class BehaviourTreeEditor : EditorWindow
{
    public IAFlyweight iaFlyweight;
    private bool Reiniciando = false;

    public enum RegularTasks { LOGIC, DECORATOR, SELECTOR, SEQUENCE, CONDITIONAL, ACTION }
    public RegularTasks actualRegularTask;
    public enum ConditionalTasks { IsMineEmpty, IsWorkerEmpty, IsWorkerFull, IsNearMine }
    public ConditionalTasks actualConditionalTask;
    public enum ActionTasks { Depositing, GoToBase, GoToMine, Mining }
    public ActionTasks actualActionTask;
    public enum LogicTasks { OR, AND }
    public LogicTasks actualLogicTask;
    public enum DecoratorTasks { INVERSOR }
    public DecoratorTasks actualDecoratorTask;

    public BehaviourTree.Task selectedTask;

    [MenuItem("Window/Behaviour Tree Editor")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(BehaviourTreeEditor));
    }

    private void OnFocus()
    {
        if (iaFlyweight == null)
        {
            selectedTask = null;
            Reiniciando = false;
        }
    }

    private void OnGUI()
    {
        iaFlyweight = (IAFlyweight)EditorGUILayout.ObjectField("Label:", iaFlyweight, typeof(IAFlyweight), true);
        if (iaFlyweight)
            actualRegularTask = (RegularTasks)EditorGUILayout.EnumPopup("Type Task to Create", actualRegularTask);
        else
        {
            EditorGUILayout.LabelField("Missing:", "Add IAFlyweight object");
            return;
        }

        if (!ReiniciandoIaFlyweight())
            return;

        TaskToCreate(actualRegularTask);
        ShowTasks();
    }

    bool ReiniciandoIaFlyweight()
    {
        if (iaFlyweight.root == null)
        {
            if (GUILayout.Button("Inicializar IAFlyWeight"))
            {
                iaFlyweight.root = new Sequence();
                selectedTask = iaFlyweight.root;
            }
            return false;
        }
        else
        {
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("¿Reniciar?", GUILayout.Width(100)) || Reiniciando)
            {
                Reiniciando = true;
                if (GUILayout.Button("¿Seguro?", GUILayout.Width(100)))
                {
                    iaFlyweight.root = null;
                    selectedTask = null;
                    Reiniciando = false;
                    EditorGUILayout.EndHorizontal();
                    return false;
                }
                if (GUILayout.Button("Volver", GUILayout.Width(100)))
                {
                    Reiniciando = false;
                }
                EditorGUILayout.EndHorizontal();
                return false;
            }
            EditorGUILayout.EndHorizontal();
            return true;
        }
    }

    void TaskToCreate(RegularTasks actualTask)
    {
        BehaviourTree.Task task = null;
        switch (actualTask)
        {
            case RegularTasks.CONDITIONAL:
                actualConditionalTask = (ConditionalTasks)EditorGUILayout.EnumPopup("Task to Create", actualConditionalTask);
                if (GUILayout.Button("Add Conditional"))
                {
                    switch (actualConditionalTask)
                    {
                        case ConditionalTasks.IsMineEmpty:
                            task = new IsMineEmpty();
                            break;
                        case ConditionalTasks.IsWorkerEmpty:
                            task = new IsWorkerEmpty();
                            break;
                        case ConditionalTasks.IsWorkerFull:
                            task = new IsWorkerFull();
                            break;
                        case ConditionalTasks.IsNearMine:
                            task = new IsNearMine();
                            break;
                        default:
                            break;
                    }
                }
                break;

            case RegularTasks.ACTION:
                if (selectedTask != null && (selectedTask.taskBaseType == "Logic"))
                {
                    EditorGUILayout.LabelField("Cant add this type of Task");
                    return;
                }
                actualActionTask = (ActionTasks)EditorGUILayout.EnumPopup("Task to Create", actualActionTask);
                if (GUILayout.Button("Add Action"))
                {
                    switch (actualActionTask)
                    {
                        case ActionTasks.Depositing:
                            task = new Depositing();
                            break;
                        case ActionTasks.GoToBase:
                            task = new GoToBase();
                            break;
                        case ActionTasks.GoToMine:
                            task = new GoToMine();
                            break;
                        case ActionTasks.Mining:
                            task = new Mining();
                            break;
                        default:
                            break;
                    }
                }
                break;

            case RegularTasks.LOGIC:
                if (selectedTask != null && (selectedTask.taskBaseType == "Conditional" || selectedTask.taskBaseType == "Action" || selectedTask.taskBaseType == "Logic" || selectedTask.taskBaseType == "Decorator"))
                {
                    EditorGUILayout.LabelField("Cant add this type of Task");
                    return;
                }
                actualLogicTask = (LogicTasks)EditorGUILayout.EnumPopup("Task to Create", actualLogicTask);
                if (GUILayout.Button("Add Logic"))
                {
                    switch (actualLogicTask)
                    {
                        case LogicTasks.OR:
                            task = new OrTask();
                            break;
                        case LogicTasks.AND:
                            task = new AndTask();
                            break;
                        default:
                            break;
                    }
                }
                break;
            case RegularTasks.DECORATOR:
                if (selectedTask != null && (selectedTask.taskBaseType == "Conditional" || selectedTask.taskBaseType == "Action" || selectedTask.taskBaseType == "Decorator"))
                {
                    EditorGUILayout.LabelField("Cant add this type of Task");
                    return;
                }
                actualDecoratorTask = (DecoratorTasks)EditorGUILayout.EnumPopup("Task to Create", actualDecoratorTask);
                if (GUILayout.Button("Add Decorator"))
                {
                    switch (actualDecoratorTask)
                    {
                        case DecoratorTasks.INVERSOR:
                            task = new Inversor();
                            break;
                        default:
                            break;
                    }
                }
                break;
            case RegularTasks.SELECTOR:
                if (selectedTask != null && (selectedTask.taskBaseType == "Conditional" || selectedTask.taskBaseType == "Action" || selectedTask.taskBaseType == "Logic" || selectedTask.taskBaseType == "Decorator"))
                {
                    EditorGUILayout.LabelField("Cant add this type of Task");
                    return;
                }
                if (GUILayout.Button("Add Selector"))
                {
                    task = new Selector();
                }
                break;
            case RegularTasks.SEQUENCE:
                if (selectedTask != null && (selectedTask.taskBaseType == "Conditional" || selectedTask.taskBaseType == "Action" || selectedTask.taskBaseType == "Logic" || selectedTask.taskBaseType == "Decorator"))
                {
                    EditorGUILayout.LabelField("Cant add this type of Task");
                    return;
                }
                if (GUILayout.Button("Add Sequence"))
                {
                    task = new Sequence();
                }
                break;
            default:
                break;
        }

        if (task != null)
        {
            task.parent = selectedTask;
            selectedTask.tasks.Add(task);
        }

    }

    void ShowTasks()
    {
        if (selectedTask == null)
        {
            selectedTask = iaFlyweight.root;
        }
        if (iaFlyweight.root.tasks == null)
            return;

        EditorGUILayout.BeginHorizontal();

        if (selectedTask.parent != null)
        {
            if (GUILayout.Button("Parent: " + selectedTask.GetType().ToString()))
            {
                selectedTask = selectedTask.parent;
            }
            if (selectedTask.tasks == null || (selectedTask.tasks != null && selectedTask.tasks.Count == 0))
            {
                if (GUILayout.Button("Remove actual Task"))
                {
                    BehaviourTree.Task auxTask = selectedTask.parent;
                    selectedTask.parent = null;
                    auxTask.tasks.Remove(selectedTask);
                    selectedTask = auxTask;
                }
            }
        }
        else
            GUILayout.Label("Sequence Root Task");

        if (selectedTask.tasks == null) return;
        foreach (var task in selectedTask.tasks)
        {
            //action and condition doesnt have child tasks
            if (task.taskBaseType != "Action" && task.taskBaseType != "Conditional")
            {
                if (GUILayout.Button(task.GetType().ToString() + ": " + task.tasks.Count))
                {
                    selectedTask = task;
                }
            }
            else if (GUILayout.Button(task.GetType().ToString()))
            {
                selectedTask = task;
            }
        }
    }
}
