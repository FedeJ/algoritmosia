﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorFlyweight : ScriptableObject {
    public string Name;
    public Material color;
    public int type;
    public int weight;
}
